module gitee.com/appfan/demo

go 1.14

require (
	github.com/disintegration/imaging v1.6.2
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8
	rsc.io/qr v0.2.0
	rsc.io/rsc v0.0.0-20180427141835-fc6202590229
)
