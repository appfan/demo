package main

import (
	"encoding/json"
	"image"
	"image/jpeg"
	"image/png"
	"log"
	"os"
	"strings"

	"github.com/disintegration/imaging"
)

func main() {
	log.SetFlags(log.Ltime | log.Ldate | log.Lshortfile)
	var datas []*struct {
		Name     string `json:"name"`
		ImageURL string `json:"image_url"`
	}
	json.Unmarshal([]byte(data), &datas)
	for _, data := range datas {
		if data.ImageURL != "" {
			fname := strings.Replace(data.Name, "/", "_", -1)
			path := strings.Split(data.ImageURL, ".")
			var name string
			if len(path) > 0 {
				name = path[len(path)-1]
				oldpath := "images/" + fname + "." + name
				newpaths := strings.Split(data.ImageURL, "/")
				if len(newpaths) > 1 {
					newpath := "images_corp/new_bai2_" + newpaths[len(newpaths)-1]
					log.Println(oldpath, newpath)
					cropImage(oldpath, newpath, name)
				}
			}
		}
	}
}

func cropImage(oldpath, newpath, extName string) {
	f, err := os.Open(oldpath)
	if err != nil {
		log.Println(err)
		return
	}
	var img image.Image

	if extName == "png" {
		img, err = png.Decode(f)
	} else if extName == "jpg" {
		img, err = jpeg.Decode(f)
	}
	if err != nil {
		log.Println(err)
		return
	}
	f.Close()

	if img.Bounds().Size().X > 210 {
		img = imaging.Resize(img, 210, 0, imaging.Lanczos)
	}
	imaging.Save(img, newpath)
}

const data = `[{
	"name": "两鲜产品分类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1461562362471.png"
},
{
	"name": "肉干\/肉脯",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1513852718362.jpg"
},
{
	"name": "肉松\/鱼松",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1516174762581.jpg"
},
{
	"name": "快手菜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1515122597147.jpg"
},
{
	"name": "其它水果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1465293535175.jpg"
},
{
	"name": "海鲜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462864359894.png"
},
{
	"name": "冰鲜肉类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1520405594712.jpg"
},
{
	"name": "白酒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1515553487835.jpg"
},
{
	"name": "洋酒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1519711297388.jpg"
},
{
	"name": "生鲜礼盒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1520578143677.jpg"
},
{
	"name": "碳酸饮料",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1520904185170.jpg"
},
{
	"name": "蜂蜜\/糖浆",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1482302306910.jpg"
},
{
	"name": "梨",
	"image_url": null
},
{
	"name": "蛋糕甜点",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1521187664794.jpg"
},
{
	"name": "烘焙食材",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1481248368333.jpg"
},
{
	"name": "牛羊肉类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1512223927478.jpg"
},
{
	"name": "快手海鲜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1519724323237.jpg"
},
{
	"name": "鲳鱼",
	"image_url": null
},
{
	"name": "零食礼盒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1520579491024.jpg"
},
{
	"name": "莲雾",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873487875.jpg"
},
{
	"name": "方便面",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1512117309557.jpg"
},
{
	"name": "凉菜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1521187163856.jpg"
},
{
	"name": "酒饮礼盒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1520579532584.jpg"
},
{
	"name": "海鲜水产类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1512025303473.jpg"
},
{
	"name": "水",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462876313906.jpg"
},
{
	"name": "田园蔬菜类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1512025314907.jpg"
},
{
	"name": "粮油礼盒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1520579579156.jpg"
},
{
	"name": "咖啡",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1472004321203.jpg"
},
{
	"name": "枣",
	"image_url": null
},
{
	"name": "日用",
	"image_url": null
},
{
	"name": "日本酒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1504490399327.jpg"
},
{
	"name": "丸子滑类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1512025328118.jpg"
},
{
	"name": "雪糕",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1470638414439.jpg"
},
{
	"name": "鲜花礼盒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1520579616130.jpg"
},
{
	"name": "柿子",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471842850101.jpg"
},
{
	"name": "狗粮",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1504490687881.jpg"
},
{
	"name": "底料蘸料",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1512027093993.jpg"
},
{
	"name": "冰棒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1470639503371.jpg"
},
{
	"name": "火锅饮品",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1512025364408.jpg"
},
{
	"name": "海淘其他",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1478741313031.jpg"
},
{
	"name": "饭后果盘",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1512025388253.jpg"
},
{
	"name": "奇异莓",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1504583904650.jpg"
},
{
	"name": "甜巧克力",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1482303890981.jpg"
},
{
	"name": "荔枝",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873279109.jpg"
},
{
	"name": "粽子",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1492693242288.png"
},
{
	"name": "保健品",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1468490773282.jpg"
},
{
	"name": "带鱼",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471504797162.jpg"
},
{
	"name": "厨房电器",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1481083488761.jpg"
},
{
	"name": "枇杷",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873330761.jpg"
},
{
	"name": "烘焙工具",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1504490217719.jpg"
},
{
	"name": "蛋糕甜点",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1468569722100.jpg"
},
{
	"name": "杨梅",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1465785998749.jpg"
},
{
	"name": "多籽",
	"image_url": null
},
{
	"name": "水饺",
	"image_url": null
},
{
	"name": "http:\/\/www.freshfresh.co",
	"image_url": null
},
{
	"name": "糖果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462876229706.jpg"
},
{
	"name": "http:\/\/www.freshfresh.co",
	"image_url": null
},
{
	"name": "山竹",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873416904.jpg"
},
{
	"name": "保健品",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1478678380200.jpg"
},
{
	"name": "海鲜礼品\/组合",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1464405671335.jpg"
},
{
	"name": "披萨三明治",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462875203819.jpg"
},
{
	"name": "巧克力",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462875412700.jpg"
},
{
	"name": "石斑鱼",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1480047828689.jpg"
},
{
	"name": "马肉",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1499690019486.jpg"
},
{
	"name": "海参",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1480049431666.jpg"
},
{
	"name": "儿童",
	"image_url": null
},
{
	"name": "枸杞",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1499754029426.jpg"
},
{
	"name": "甜点",
	"image_url": null
},
{
	"name": "山楂",
	"image_url": null
},
{
	"name": "调味",
	"image_url": null
},
{
	"name": "鸭肉类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1480048701049.jpg"
},
{
	"name": "火锅用料",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1480041823969.jpg"
},
{
	"name": "释迦",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462874104307.jpg"
},
{
	"name": "圣女果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873128052.jpg"
},
{
	"name": "西梅",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1470973875430.jpg"
},
{
	"name": "高端肉类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1490231949097.jpg"
},
{
	"name": "甜蜂蜜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1482302899720.jpg"
},
{
	"name": "酱油醋",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471512699660.jpg"
},
{
	"name": "月饼",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1501727947950.jpg"
},
{
	"name": "海苔\/紫菜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1506678320573.jpg"
},
{
	"name": "龟苓膏",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1507712928629.jpg"
},
{
	"name": "草莓",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873268226.jpg"
},
{
	"name": "调味料",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1504679964974.jpg"
},
{
	"name": "车厘子",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873013233.jpg"
},
{
	"name": "榴莲",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1464327958777.jpg"
},
{
	"name": "虾仁",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471339778906.jpg"
},
{
	"name": "供港蔬菜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1504712562915.jpg"
},
{
	"name": "坚果类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1504490254611.jpg"
},
{
	"name": "汤圆酒酿",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471048667760.jpg"
},
{
	"name": "三文鱼",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462874237408.jpg"
},
{
	"name": "海淘母婴",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1488444887413.jpg"
},
{
	"name": "冰激凌模具",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1499780685337.jpg"
},
{
	"name": "其它零食",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1464328282480.jpg"
},
{
	"name": "椰子",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873028070.jpg"
},
{
	"name": "鳕鱼",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462874693815.jpg"
},
{
	"name": "葡萄酒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1464328245907.jpg"
},
{
	"name": "黄酒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1506494277839.jpg"
},
{
	"name": "新奇特！",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1480050102951.jpg"
},
{
	"name": "鲜花\/盆栽",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462876374714.jpg"
},
{
	"name": "红虾",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462874677272.jpg"
},
{
	"name": "贝类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462874646500.jpg"
},
{
	"name": "芽菜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1505712029159.jpg"
},
{
	"name": "刺身类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462875278567.jpg"
},
{
	"name": "桃李杏枣",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1513865963158.jpg"
},
{
	"name": "柠檬",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873291473.jpg"
},
{
	"name": "龙虾",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1473643244809.jpg"
},
{
	"name": "牛排",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1478245017327.jpg"
},
{
	"name": "酸奶类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1468569834354.jpg"
},
{
	"name": "香肠培根",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462875168832.jpg"
},
{
	"name": "牛肉类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462875044903.jpg"
},
{
	"name": "豆制品",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1510907838734.jpg"
},
{
	"name": "小龙虾",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1501819721061.jpg"
},
{
	"name": "果汁",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1472006509921.jpg"
},
{
	"name": "火龙果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873392473.jpg"
},
{
	"name": "猫粮",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1488444902602.jpg"
},
{
	"name": "豆乳\/奶茶",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1511420611442.jpg"
},
{
	"name": "水果礼品\/组合",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471339005854.jpg"
},
{
	"name": "黄鱼",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471505932438.jpg"
},
{
	"name": "有机蔬菜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1487823442530.jpg"
},
{
	"name": "芒果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1514455618194.jpg"
},
{
	"name": "鲜奶类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462875316005.jpg"
},
{
	"name": "干货",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462929961618.jpg"
},
{
	"name": "梨",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1478244862896.jpg"
},
{
	"name": "豆浆",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1479448885749.jpg"
},
{
	"name": "食用油",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462877490658.jpg"
},
{
	"name": "糕点\/饼干",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1506678962864.jpg"
},
{
	"name": "杂粮",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1488251145159.jpg"
},
{
	"name": "海淘日用",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1488444861546.jpg"
},
{
	"name": "工具",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1464327904471.jpg"
},
{
	"name": "蛋类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462875295718.jpg"
},
{
	"name": "香蕉",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462872997493.jpg"
},
{
	"name": "膨化\/薯片",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1463568109274.jpg"
},
{
	"name": "半成品菜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471048993448.jpg"
},
{
	"name": "麦片",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471424764269.jpg"
},
{
	"name": "缸鸭狗",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1472118224651.jpg"
},
{
	"name": "米面",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1490067411678.jpg"
},
{
	"name": "根茎类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873830605.jpg"
},
{
	"name": "石榴",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471274332889.jpg"
},
{
	"name": "无花果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1468401561946.jpg"
},
{
	"name": "女士酒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1497594368712.jpg"
},
{
	"name": "菌菇类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873934250.jpg"
},
{
	"name": "海淘保健品",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1478678938610.jpg"
},
{
	"name": "其它鱼类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462874831995.jpg"
},
{
	"name": "生蚝",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1506576923990.jpg"
},
{
	"name": "时蔬组合包",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873810861.jpg"
},
{
	"name": "姜葱蒜椒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873790694.jpg"
},
{
	"name": "猪肉类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462875094032.jpg"
},
{
	"name": "羊肉类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462875070031.jpg"
},
{
	"name": "常温奶类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1468421878201.jpg"
},
{
	"name": "海淘个护",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1497512316095.jpg"
},
{
	"name": "黄油\/乳酪",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462875357621.jpg"
},
{
	"name": "其它禽类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1464328012506.jpg"
},
{
	"name": "礼品卡券",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1520579708807.jpg"
},
{
	"name": "鳗鱼",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1480044670986.jpg"
},
{
	"name": "饮料",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1472006402590.jpg"
},
{
	"name": "橙子",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462872986383.jpg"
},
{
	"name": "柚子",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873405288.jpg"
},
{
	"name": "茶叶",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1504490304490.jpg"
},
{
	"name": "柑橘",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1480046720590.jpg"
},
{
	"name": "海淘食品",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1497604115683.jpg"
},
{
	"name": "牛油果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873438711.jpg"
},
{
	"name": "高端食材",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1490231867812.png"
},
{
	"name": "保健",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1468576307002.png"
},
{
	"name": "外卖",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1468576298135.png"
},
{
	"name": "粮油",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462864442406.png"
},
{
	"name": "海鲜干货",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1468569815906.jpg"
},
{
	"name": "雪莲果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471605811398.jpg"
},
{
	"name": "酒饮",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462864432956.png"
},
{
	"name": "火锅",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1480569478396.png"
},
{
	"name": "厨卫清洁",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1507862194548.jpg"
},
{
	"name": "肉类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462864376471.png"
},
{
	"name": "茄果类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873886727.jpg"
},
{
	"name": "全球购",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1478752893674.png"
},
{
	"name": "零食",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462864421672.png"
},
{
	"name": "礼盒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462864471110.png"
},
{
	"name": "烘焙",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1491887650113.png"
},
{
	"name": "厨房",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462864453418.png"
},
{
	"name": "冰激凌 ",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1469182792349.png"
},
{
	"name": "蟹类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1513953079862.jpg"
},
{
	"name": "其它冰激凌",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1470641409006.jpg"
},
{
	"name": "包子馒头",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1490347157959.jpg"
},
{
	"name": "豆类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873906674.jpg"
},
{
	"name": "宠物",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1491877558399.png"
},
{
	"name": "丸子",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462875186463.jpg"
},
{
	"name": "速食",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462864395165.png"
},
{
	"name": "水果礼盒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1520577977648.jpg"
},
{
	"name": "蔬菜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462864349432.png"
},
{
	"name": "其它工具",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1507862277602.jpg"
},
{
	"name": "叶菜类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873756913.jpg"
},
{
	"name": "蛋奶",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462864410881.png"
},
{
	"name": "其它海鲜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1519724716094.jpg"
},
{
	"name": "金枪鱼",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1470983069162.jpg"
},
{
	"name": "Disney",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471343240088.jpg"
},
{
	"name": "水果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462864337252.png"
},
{
	"name": "议案了",
	"image_url": null
},
{
	"name": "紫色蔬菜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1513074199119.jpg"
},
{
	"name": "鹅肝",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1473661538109.jpg"
},
{
	"name": "蓝莓树莓",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873309990.jpg"
},
{
	"name": "水饺馄饨",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471048541443.jpg"
},
{
	"name": "意大利面",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1506499174325.jpg"
},
{
	"name": "速冻\/熟食",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462875150743.jpg"
},
{
	"name": "糖果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1506677941941.jpg"
},
{
	"name": "苹果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873572667.jpg"
},
{
	"name": "蜜饯\/果干",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1506679061834.jpg"
},
{
	"name": "冷冻蔬菜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1470642465614.jpg"
},
{
	"name": "燕窝",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1507712944427.jpg"
},
{
	"name": "沙拉",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1515060711843.jpg"
},
{
	"name": "龙眼",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462872866321.jpg"
},
{
	"name": "奶粉",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1507627117835.jpg"
},
{
	"name": "释迦",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1480047326369.jpg"
},
{
	"name": "糖盐",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471512416742.jpg"
},
{
	"name": "瓜类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1468569247191.jpg"
},
{
	"name": "鱿鱼\/墨鱼",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1487137632524.jpg"
},
{
	"name": "奇异果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873204971.jpg"
},
{
	"name": "虾籽鱼籽",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1480049647583.jpg"
},
{
	"name": "其它甜品",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1506481868263.jpg"
},
{
	"name": "Gooble",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471509871729.jpg"
},
{
	"name": "啤酒",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1504679997781.jpg"
},
{
	"name": "烘焙工具",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1507861823394.jpg"
},
{
	"name": "厨房用具",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1507862068564.jpg"
},
{
	"name": "净化器",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1507863327312.jpg"
},
{
	"name": "芽",
	"image_url": null
},
{
	"name": "巧克力",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1512117623098.jpg"
},
{
	"name": "凤梨\/菠萝",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462873072647.jpg"
},
{
	"name": "柿子",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1480047546706.jpg"
},
{
	"name": "芭乐",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1512906483822.jpg"
},
{
	"name": "其他海鲜",
	"image_url": null
},
{
	"name": "小龙虾1",
	"image_url": null
},
{
	"name": "猪肉类",
	"image_url": null
},
{
	"name": "烘焙推荐",
	"image_url": null
},
{
	"name": "烘焙",
	"image_url": null
},
{
	"name": "鲳鱼",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471504667726.jpg"
},
{
	"name": "杂粮",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1464328359690.jpg"
},
{
	"name": "海淘零食",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1487920025422.jpg"
},
{
	"name": "高端海鲜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1490232139164.jpg"
},
{
	"name": "其他食材",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1490233241559.jpg"
},
{
	"name": "俄罗斯冰激凌",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1496746657276.jpg"
},
{
	"name": "泡菜",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1462875267578.jpg"
},
{
	"name": "树莓",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1464327944988.jpg"
},
{
	"name": "肉类礼品\/组合",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1464361541123.jpg"
},
{
	"name": "高端水果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1490232783235.jpg"
},
{
	"name": "面包",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1504679797515.jpg"
},
{
	"name": "咖喱",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471512571701.jpg"
},
{
	"name": "白虾",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471339475514.jpg"
},
{
	"name": "冲饮",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1468460477349.jpg"
},
{
	"name": "鸡肉类",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1465293853478.jpg"
},
{
	"name": "提子\/葡萄",
	"image_url": "http:\/\/qiniu.freshfresh.com\/odoo_croped_ff_app_3_0_1516354891604.jpg"
},
{
	"name": "百香果",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1468401545090.jpg"
},
{
	"name": "黑莓",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1471338943981.jpg"
},
{
	"name": "面条",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1504489950211.jpg"
},
{
	"name": "桶装",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1470638398030.jpg"
},
{
	"name": "新奇特！",
	"image_url": null
},
{
	"name": "猪肉",
	"image_url": null
},
{
	"name": "外卖",
	"image_url": "http:\/\/qiniu.freshfresh.com\/ff_app_3_0_1468406875247.jpg"
},
{
	"name": "其他",
	"image_url": null
}
]`
