package main

import (
	"bytes"
	"image"
	"image/color"
	"image/draw"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/bmp"
	"golang.org/x/image/font"
	"golang.org/x/image/math/fixed"
)

var (
	fontFile  = "config/PingFang_Medium.ttf"
	fontStyle *truetype.Font
	fontSize  float64 = 22
	fontDPI   float64 = 72

	colorA               = [3]float64{255, 162, 31}
	colorB               = [3]float64{255, 92, 45}
	textCornerMarkCorner = getBGImage("http://qiniu.freshfresh.com/textcornermark_corner.png")
)

func init() {
	fontBytes, err := ioutil.ReadFile(fontFile)
	if err != nil {
		log.Println(err)
		return
	}
	fontStyle, err = freetype.ParseFont(fontBytes)
	if err != nil {
		log.Println(err)
		return
	}
}

func main() {
	http.HandleFunc("/textcornermark", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "image/png")
		png.Encode(w, textCornerMark(req.FormValue("text")))
	})
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func linearGradient(x, max float64) (uint8, uint8, uint8) {
	d := x / max
	r := colorA[0] + d*(colorB[0]-colorA[0])
	g := colorA[1] + d*(colorB[1]-colorA[1])
	b := colorA[2] + d*(colorB[2]-colorA[2])
	return uint8(r), uint8(g), uint8(b)
}

func textCornerMark(text string) *image.NRGBA {
	img := image.NewNRGBA(image.Rect(0, 0, 220, 28))

	d := &font.Drawer{
		Dst: img,
		Src: image.NewUniform(color.RGBA{0xff, 0xff, 0xff, 0xff}),
		Face: truetype.NewFace(fontStyle, &truetype.Options{
			Size:    fontSize * 1,
			DPI:     fontDPI,
			Hinting: font.HintingNone,
		}),
	}
	width := d.MeasureString(text).Round() + 18

	for x := 0; x < width; x++ {
		for y := 0; y < 28; y++ {
			r, g, b := linearGradient(float64(x), float64(width))
			c := color.NRGBA{r, g, b, 255}
			img.Set(x, y, c)
		}
	}
	draw.Draw(img, textCornerMarkCorner.Bounds().Add(image.Pt(width, 0)), textCornerMarkCorner, image.ZP, draw.Over)

	d.Dot = fixed.Point26_6{X: fixed.I(18), Y: fixed.I(21)+50}
	d.DrawString(text)

	return img
}

// getBGImage 获取图
func getBGImage(url string) image.Image {
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
		return image.NewNRGBA(image.Rect(0, 0, 600, 400))
	}
	defer resp.Body.Close()
	data, _ := ioutil.ReadAll(resp.Body)
	contentType := http.DetectContentType(data)
	var imgBG image.Image
	if contentType == "image/png" {
		imgBG, err = png.Decode(bytes.NewReader(data))
	} else if contentType == "image/jpeg" {
		imgBG, err = jpeg.Decode(bytes.NewReader(data))
	} else if contentType == "image/gif" {
		imgBG, err = gif.Decode(bytes.NewReader(data))
	} else if contentType == "image/bmp" {
		imgBG, err = bmp.Decode(bytes.NewReader(data))
	}
	if err != nil {
		log.Println(err)
		return image.NewNRGBA(image.Rect(0, 0, 600, 400))
	}
	return imgBG
}
