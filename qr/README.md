
很早之前的代码分享出来。

朋友圈被封后只能分享图片到朋友圈。我们做过各种动态生成的图片，主要是二维码，还有web图的截取（比如菜谱）。

这里分享一下golang生成二维码、圆形头像和昵称的合成图片。

用到的库主要是下面这些。

```go
	"image"
	
	"golang.org/x/image/font"
	"golang.org/x/image/math/fixed"
	"github.com/golang/freetype/truetype"
	"rsc.io/qr"
	"rsc.io/rsc/qr/web/resize"
```

image在GOROOT下
code.google.com/p/rsc下载地址 https://github.com/yinshuwei/rsc
golang.org/x/image 下载地址https://github.com/golang/image
github.com/golang/freetype/truetype 下载地址https://github.com/golang/freetype

代码在
https://git.oschina.net/appfan/demo/ 下的qr中

程序中背景图是

![背景图](http://qiniu.nongtianlang.com/qr/bg.png)

库处理好后就可以运行了

```shell
go run main.go
```

用于生成图片的数据

```json
{"url":"https://www.nongtianlang.com/","headurl":"http://qiniu.nongtianlang.com/qr/head.jpg","nickname":"农天狼"}
```

头像 http://qiniu.nongtianlang.com/qr/head.jpg
<img src="http://qiniu.nongtianlang.com/qr/head.jpg" width="120"/>
二维码地址 https://www.nongtianlang.com/

昵称 农天狼

base64后放到请求的data中

访问 http://localhost:8080/qr?data=eyJ1cmwiOiJodHRwOi8vd3d3LmFwcGZhbi5jYy8iLCJoZWFkdXJsIjoiaHR0cDovL3Fpbml1Lm5vbmd0aWFubGFuZy5jb20vcXIvaGVhZC5qcGciLCJuaWNrbmFtZSI6IuWGnOWkqeeLvCJ9

可以看到如下效果

![合成后的结果](http://qiniu.nongtianlang.com/qr/result.jpg)
